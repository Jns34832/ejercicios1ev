package unidad4;

import java.util.Random;
import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		Random valor=new Random(); 
		int n=0;
		int tirar=0;
		System.out.println("Cuantas veces quiere lanzar el dado? ");
		n=teclado.nextInt();
		int[] resultados=new int[7];
		for (int i=0; i<n; i++) {
			tirar=valor.nextInt(6)+1;
			
			resultados[tirar]++;
		}
		for (int i=1; i<7; i++) {
			System.out.println("El n�mero "+ i+ " ha salido "+resultados[i]+" veces");
		}
		
	}

}
