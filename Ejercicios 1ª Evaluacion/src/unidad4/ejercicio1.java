package unidad4;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		
		Scanner teclado =new Scanner(System.in);
		int numcaracteres=0;		
		int i;
		int vocala,vocale,vocali,vocalo,vocalu;
		vocala=vocale=vocali=vocalo=vocalu=0;
		
		System.out.println("Introduzca la cadena para saber el numero de vocales: ");
		String frase=teclado.nextLine();    //pedir la frase por teclado
	
		numcaracteres=frase.length();	//guardar el numero de caracteres de la frase
	
		frase = frase.toUpperCase();	//convertir la frase a mayusculas
		
		for (i=0; i<numcaracteres; i++) {	//recorrer la frase para saber si es vocal
			
			switch (frase.charAt(i)) {      //contamos las vocales
				case 'A':
					vocala++;
					break;
				case 'E':
					vocale++;
					break;
				case 'I':
					vocali++;
					break;
				case 'O':
					vocalo++;
					break;
				case 'U':
					vocalu++;
					break;
				default:
					break;
			}
		}
		System.out.println("La frase contiene las siguientes vocales: ");
		System.out.println("Vocal A: " + vocala);
		System.out.println("Vocal E: " + vocale);
		System.out.println("Vocal I: " + vocali);
		System.out.println("Vocal O: " + vocalo);
		System.out.println("Vocal U: " + vocalu);
	}
}
