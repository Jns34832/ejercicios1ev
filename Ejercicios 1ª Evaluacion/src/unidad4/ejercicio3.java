package unidad4;

import java.util.Scanner;

public class ejercicio3 {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		
		String cadena;
		String cadenabuscar;
	
		int encontrado=0;
		int posicion=0;
		int inicio=0;
		
		System.out.println("Introduce una cadena: ");
		cadena=teclado.nextLine();

		System.out.println("Introduce la cadena a buscara: ");
		cadenabuscar=teclado.nextLine();
				
		int fin=cadena.length();
		
		while (inicio<=fin) {
			cadena=cadena.substring(inicio,fin);
			posicion=cadena.indexOf(cadenabuscar);		//da la posicion de la cadenabuscar
			
			if (posicion!=-1) encontrado++;
			inicio=cadenabuscar.length()+posicion++;
			fin=cadena.length();
		}
		System.out.println("La cadena "+ cadenabuscar+" se ha encontrado: "+encontrado + " veces");
	}
}
