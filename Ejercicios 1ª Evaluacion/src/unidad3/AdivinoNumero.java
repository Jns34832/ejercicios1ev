package unidad3;
	import java.util.Random;
	import java.util.Scanner;
public class AdivinoNumero {
	public static void main(String[] args) {
		
		Scanner teclado=new Scanner (System.in);{
		
		Random valor = new Random();
		int maximo=0;
		int liminf=0;
		int limsup=0;
		int adivina=0;
		String numero;

		System.out.print("Cual es el valor del limite superior?");
		maximo=teclado.nextInt();
		
		System.out.printf("He pensado un n�mero entre 1 y %d, adivina cual es...",maximo);
		System.out.println(" ");
		liminf=1;
		limsup=maximo++;
		adivina=valor.nextInt(maximo) +1;

		do {
			System.out.printf("Es el %d? ",adivina);
			System.out.println(" ");
			System.out.println("mayor, menor, acertado? ");
			numero=teclado.next();
			if (numero.equalsIgnoreCase("mayor")) liminf=adivina;
			if (numero.equalsIgnoreCase("menor")) limsup=adivina;
			adivina=valor.nextInt(limsup-liminf)+liminf;
		} while (!numero.equalsIgnoreCase("acertado"));

		System.out.println("HE ACERTADO!!!");	
	}
	}
}
