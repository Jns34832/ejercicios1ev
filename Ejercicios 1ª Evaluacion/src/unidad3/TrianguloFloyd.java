package unidad3;

import java.util.Scanner;

public class TrianguloFloyd {

	public static void main(String[] args) {
	
		Scanner teclado=new Scanner(System.in);
		int i,j, filas;
		
		System.out.println("De cuantas filas quieres el triangulo de Floyd?");
		filas=teclado.nextInt();
		int num=1;
		for (i=1; i<=filas; i++) {
			for (j=1; j<=i; j++) {
			 System.out.print(num + " ");
			 num++;
			}
			System.out.println();
		}
	}
}
