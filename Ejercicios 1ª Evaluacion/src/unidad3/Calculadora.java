package unidad3;
import java.util.Scanner;
public class Calculadora {
	public static void main(String[] args) {
		Scanner teclado=new Scanner (System.in);
		Integer numero1, numro2;
		String resp;
		System.out.print("Desea realizar alguna operacion? ");
		resp = teclado.next();
		while (resp.equalsIgnoreCase("S")) {
			System.out.println();
			System.out.print(" Que operacion desea realizar? ");
			String op=teclado.next();
			System.out.print("Introduzca el primer operador: ");
			numero1=teclado.nextInt();
			System.out.print("Introduzca el segundo operador: ");
			numro2=teclado.nextInt();
			System.out.println();
			switch (op) {
				case "+":
					System.out.print(numero1 +" "+ op +" "+ numro2 +" = " + (numero1+numro2));
					break;
				case "-":
					System.out.print(numero1 +" "+ op +" "+ numro2 +" = " + (numero1-numro2));
					break;
				case "*":
					System.out.print(numero1 +" "+ op +" "+ numro2 +" = " + (numero1*numro2));
					break;
				case "/":
					System.out.print(numero1 +" "+ op +" "+ numro2 +" = " + (numero1/numro2));
					break;	
				default:
					System.out.print("No s� hacer la operaci�n "+op);
			}
		System.out.println();
		System.out.println();
		System.out.print("Desea realizar otra operacion? ");
		resp = teclado.next();
		}	
	}

}
