package unidad3;
import java.util.Random;
import java.util.Scanner;
public class AdivinaNumero {
	public static void main(String[] args) {
		Scanner teclado=new Scanner (System.in);
		Random valor = new Random();
		int maximo;
		int adivina;
		int numero;
		maximo=valor.nextInt(99000)+1000;
		adivina=valor.nextInt(maximo) +1;
		System.out.println("He pensado un n�mero entre 1 y 100.000, adivina cual es...");
		do	{
			System.out.println(" ");
			System.out.println("Introduce un n�mero: ");
			numero=teclado.nextInt();
			if (numero<adivina) 
				System.out.println("Mi n�mero es mayor");
			if (numero>adivina) 
				System.out.println("Mi n�mero es menor");
		}while (numero!=adivina); 
		System.out.println("HAS ACERTADO!!!");	
	}
}
