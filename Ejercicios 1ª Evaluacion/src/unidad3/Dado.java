package unidad3;

import java.util.Random;
import java.util.Scanner;

public class Dado {

	public static void main(String[] args) {

		Scanner teclado=new Scanner(System.in);
		Random valor = new Random();
		
		int veces, i, dado;
		int num1,num2,num3,num4,num5,num6;
		num1=num2=num3=num4=num5=num6=0;
		//cuantas veces?
		System.out.print("Cuantas veces desea lanzar el dado? ");
		veces=teclado.nextInt();
					
		for (i=1; i<=veces; i++)
		{
			//lanza el dado
			dado=valor.nextInt(5)+1;
			System.out.print(dado+ "   ");

			//cuenta veces que sale cada cara
			switch (dado) {
				case 1:
					num1++;
					break;
				case 2:
					num2++;
					break;					
				case 3:
					num3++;
					break;					
				case 4:
					num4++;
					break;					
				case 5:
					num5++;
					break;					
				case 6:
					num6++;
					break;					
			}
		}	
		System.out.println();
		System.out.println("Han salido los siguientes resultados:");
		System.out.println("El numero 1 ha salido "+ num1+" veces");
		System.out.println("El numero 2 ha salido "+ num2+" veces");
		System.out.println("El numero 3 ha salido "+ num3+" veces");
		System.out.println("El numero 4 ha salido "+ num4+" veces");
		System.out.println("El numero 5 ha salido "+ num5+" veces");
		System.out.println("El numero 6 ha salido "+ num6+" veces");
	}
}
