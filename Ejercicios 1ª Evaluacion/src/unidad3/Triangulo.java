package unidad3;
import java.util.Scanner;
public class Triangulo {
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in); {
		
		System.out.print("Introduzca el valor del lado A: ");
		int A = teclado.nextInt();

		System.out.print("Introduzca el valor del lado B: ");
		int B = teclado.nextInt();
		
		System.out.print("Introduzca el valor del lado C: ");
		int C = teclado.nextInt();
		
		if ((A+B>C) || (A+C>B) || (B+C>A))
			System.out.print("Es un triangulo ");
		
		if ((A==B) && (B==C)) 
			System.out.print("Equilatero");
		else
			if ((A!=B) && (A!=C) && (B!=C))
				System.out.print("Escaleno");
			else
				System.out.print("Isosceles");
		}
	}
}
