package unidad3;
import java.util.Scanner;
public class Multiplicar {

	public static void main(String[] args) {
		Scanner teclado=new Scanner (System.in);
		String otra;

		do {
			System.out.print("�Que tabla deseas repasar? ");
			int tabla=teclado.nextInt();
			int fallo=0;
			for (int i=1; i<=10; i++) {
				System.out.printf("%d * %d = ", tabla, i);
				int respuesta=teclado.nextInt();
				if ((tabla*i)!=respuesta) {
					fallo++;
					System.out.println("Incorrecto. La respuesta correcta es: " + tabla * i);
				}
			}
			System.out.printf("Ha tenido %d fallos. ", fallo);
			if (fallo>=2)
				System.out.println("SUSPENDIDO");
			else
				System.out.println("APROBADO");
		
			System.out.println("Desea repasar otra tabla? S/N");
		    	otra = teclado.next();
		} while (otra.equalsIgnoreCase("S"));
		
	}

}
