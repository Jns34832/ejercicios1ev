package unidad2;

import java.util.Scanner;

public class Sueldo {

	public static void main(String[] args) {

		Scanner teclado=new Scanner(System.in);
		
		float sueldo, venta1, venta2, venta3, comisiones, total;
	
		System.out.print("Introduce el sueldo base: ");
		sueldo=teclado.nextFloat();
		System.out.print("Introduce el importe de la 1� venta: ");
		venta1=teclado.nextFloat();
		System.out.print("Introduce el importe de la 2� venta: ");
		venta2=teclado.nextFloat();
		System.out.print("Introduce el importe de la 3� venta: ");
		venta3=teclado.nextFloat();
		
		comisiones = (venta1+venta2+venta3)*10/100;
		total = sueldo + comisiones;
		System.out.printf("El importe de las comisiones por las tres ventas es: %5.2f por lo que este mes recibir� un total de %5.2f",comisiones,total);
		
	}

}
