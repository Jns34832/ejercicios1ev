package unidad2;

import java.util.Scanner;

public class Color {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner (System.in);
		int rojo, verde, azul;
		double y, i, q;
		System.out.println("Introduce en el espacio RGB las siguientes componentes: ");
		System.out.print("Rojo ");
		rojo=teclado.nextInt();
		System.out.print("Verde ");
		verde=teclado.nextInt();
		System.out.print("Azul ");
		azul=teclado.nextInt();
		
		y = 0.299 * rojo + 0.587 * verde + 0.114 * azul;
		i = 0.596 * rojo + 0.275 * verde + 0.321 * azul;
		q = 0.212 * rojo + 0.528 * verde + 0.311 * azul;
		
		System.out.printf("Estas componentes en el espacio YIQ se corresponden con: %3.3f %3.3f %3.3f",y,i,q);
			
	}

}
